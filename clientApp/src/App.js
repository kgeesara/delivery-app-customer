import React from 'react';
import './App.css';
import AppRouter from './components/RouterComponent';


function App() {
  return (
    <div className="App">
      {/* <NavigationBar />
      <WrappedHorizontalLoginForm /> */}

      {/* <LayoutCustom /> */}
      <AppRouter/>

    </div>
  );
}

export default App;
