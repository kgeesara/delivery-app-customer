
import axios from 'axios';
import AuthService from './AuthService';

const PAYMENT_API_BASE_URL = 'http://localhost:8080/pay';

class PaymentService {

    pay(payment) {
        return axios.post(PAYMENT_API_BASE_URL,payment, AuthService.getAuthHeader());
    }

}

export default new PaymentService();