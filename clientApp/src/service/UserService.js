import axios from 'axios';
import AuthService from './AuthService';

const USER_API_BASE_URL = 'http://localhost:8080/users';

class UserService {

    fetchUsers() {
        return axios.get(USER_API_BASE_URL, AuthService.getAuthHeader());
    }

    fetchUserById(userId) {
        return axios.get(USER_API_BASE_URL + '/' + userId, AuthService.getAuthHeader());
    }

    fetchUserByUserName(username) {
        axios.defaults.headers.get['Content-Type'] ='application/json;charset=utf-8';
        axios.defaults.headers.get['Access-Control-Allow-Origin'] = '*';
        return axios.get(""+USER_API_BASE_URL + '/' + username,{headers:{'Access-Control-Allow-Origin':'*'}});
    }

    deleteUser(userId) {
        return axios.delete(USER_API_BASE_URL + '/' + userId, AuthService.getAuthHeader());
    }

    addUser(user) {
        return axios.post(""+USER_API_BASE_URL+'/register', user);
    }

    editUser(user) {
        return axios.put(USER_API_BASE_URL + '/' + user.username, user, AuthService.getAuthHeader());
    }

    editUserTransactionStatus(user) {
        return axios.put(USER_API_BASE_URL + '/transactionStatus/' + user.username, user, AuthService.getAuthHeader());
    }

}

export default new UserService();