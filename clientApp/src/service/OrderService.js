import axios from 'axios';
import AuthService from './AuthService';

const ORDER_API_BASE_URL = 'http://localhost:8080/order';

class OrderService {

    saveOrder(order) {
        return axios.post(ORDER_API_BASE_URL,order, AuthService.getAuthHeader());
    }


}

export default new OrderService();