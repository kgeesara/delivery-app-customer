import axios from 'axios';
import AuthService from './AuthService';

const RESTAURANT_API_BASE_URL = 'http://localhost:8080/';

class RestaurantService {

    fetchRestaurants() {
        return axios.get(RESTAURANT_API_BASE_URL+'restaurants', AuthService.getAuthHeader());
    }

    fetchFoods() {
        return axios.get(RESTAURANT_API_BASE_URL+'foods', AuthService.getAuthHeader());
    }

}

export default new RestaurantService();