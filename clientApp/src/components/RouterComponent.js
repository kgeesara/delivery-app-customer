import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import React from "react";
import WrappedEditUserComponent from './user/EditUserComponent';
import WrappedAddUserComponent from './user/AddUserComponent';
import carousel from './home/carousel';
import WrappedHorizontalLoginForm from './user/LoginComponent';
import RestaurantFeed from './Restautant/restaurantFeed';
import FoodItems from './Restautant/FoodItems';
import Cart from './Restautant/Cart';
import WrappedPaymentForm from './payment/paymentForm';
import PrivateRoute from './privateRoute';

const AppRouter = () => {
    return(
            <Router>
                    <Switch>
                        <Route path="/" exact component={carousel} />
                        <Route path="/login" exact component={WrappedHorizontalLoginForm} />
                        <Route path="/register" component={WrappedAddUserComponent} />
                        <PrivateRoute path='/restaurantFeed' component={RestaurantFeed} />
                        <PrivateRoute exact path='/restaurantFoods' component={FoodItems} />
                        <PrivateRoute exact path='/cart' component={Cart} />
                        <PrivateRoute exact path='/editProfile' component={WrappedEditUserComponent} />
                        <PrivateRoute exact path='/payment' component={WrappedPaymentForm} />
                        {/* {JSON.parse(window.localStorage.getItem("userInfo")) ? <Route path="/restaurantFeed" component={RestaurantFeed} /> : <Route path="/" component={WrappedHorizontalLoginForm} />} */}
                        
                        
                    </Switch>
            </Router>
    )
}

export default AppRouter;