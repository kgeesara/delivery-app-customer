import React from 'react';
import { Carousel, Layout } from 'antd';
import './loginForm.css';
import NavbarBefore from '../NavbarBefore';
import CustomFooter from '../CustomFooter';

function onChange(a, b, c) {
  console.log(a, b, c);
}

class carousel extends React.Component{

  render(){
    return(
          <Layout>
   <        NavbarBefore />
            <Carousel afterChange={onChange}>
              <div>
                <h3>1</h3>
              </div>
              <div>
                <h3>2</h3>
              </div>
              <div>
                <h3>3</h3>
              </div>
              <div>
                <h3>4</h3>
              </div>
            </Carousel>
            <CustomFooter />
            </Layout>
    )
};
}


export default carousel;