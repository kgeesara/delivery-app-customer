import React from 'react';
 
import {
    Form,
    Input,
    Select,
    Button,
    Layout,
    Alert,
  } from 'antd';
import UserService from '../../service/UserService';
import NavbarBefore from '../NavbarBefore';
import CustomFooter from '../CustomFooter';
  
  const { Option } = Select;
  
  class AddUserComponent extends React.Component {

  
    constructor(props){
      super(props);
      this.state ={
          username: '',
          password: '',
          firstName: '',
          lastName: '',
          age: '',
          salary: '',
          successMessage: null,
          errorMessage: null
      }
      this.saveUser = this.saveUser.bind(this);
  }
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
  };
  // saveUser = (e) => {
  //     e.preventDefault();
  //     let user = {username: this.state.username, password: this.state.password, firstName: this.state.firstName, lastName: this.state.lastName, age: this.state.age, salary: this.state.salary};
  //     UserService.addUser(user)
  //         .then(res => {
  //             this.setState({message : 'User added successfully.'});
  //             this.props.history.push('/list-user');
  //         });
  // }

  saveUser = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      
      if (!err) {
        let user = {username: values.username, password: values.password, email: values.email, address: values.address, tpnumber: values.tpnumber};
        UserService.fetchUserByUserName(values.username)
        .then(response => {
          console.log(response);
          if(response.data.result === null){
            console.log("KKKKKKKKK");
            UserService.addUser(user)
            .then(res => {
                this.setState({successMessage : 'User added successfully.',errorMessage: null});
                // this.props.history.push('/list-user');
            });
          } else{
            console.log("LLLLLLLL");
            this.setState({errorMessage:'User name already exists', successMessage: null});
          }
        });  
      }
    });
  };

  
    handleConfirmBlur = e => {
      const { value } = e.target;
      this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    };
  
    compareToFirstPassword = (rule, value, callback) => {
      const { form } = this.props;
      if (value && value !== form.getFieldValue('password')) {
        callback('Two passwords that you enter is inconsistent!');
      } else {
        callback();
      }
    };
  
    validateToNextPassword = (rule, value, callback) => {
      const { form } = this.props;
      if (value && this.state.confirmDirty) {
        form.validateFields(['confirm'], { force: true });
      }
      callback();
    };
  
    render() {
      const { getFieldDecorator } = this.props.form;
  
      const formItemLayout = {
        labelCol: {
          xs: { span: 12 },
          sm: { span: 8 },
        },
        wrapperCol: {
          xs: { span: 12 },
          sm: { span: 8 },
        },
      };
      const tailFormItemLayout = {
        wrapperCol: {
          xs: {
            span: 24,
            offset: 0,
          },
          sm: {
            span: 8,
            offset: 8,
          },
        },
      };
      const prefixSelector = getFieldDecorator('prefix', {
        initialValue: '94',
      })(
        <Select style={{ width: 70 }}>
          <Option value="94">+94</Option>

        </Select>
      );
  
  
      return (
        <Layout className="layout">
            <NavbarBefore />
            <div style={{ background: '#fff', padding: 24 ,  height: '100%', textAlign: '-webkit-center'}}>Asiri Food Delivery
                <div>{this.state.successMessage && <Alert message={this.state.successMessage} type="success" />}{this.state.errorMessage && <Alert message={this.state.errorMessage} type="error" />}</div>
                <Form {...formItemLayout} onSubmit={this.saveUser}>

                  <Form.Item
                    label={
                      <span>
                        Username&nbsp;
                        
                      </span>
                    }
                  >
                    {getFieldDecorator('username', {
                      rules: [{ required: true, message: 'Please input your username!', whitespace: true }],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="E-mail">
                    {getFieldDecorator('email', {
                      rules: [
                        {
                          type: 'email',
                          message: 'The input is not valid E-mail!',
                        },
                        {
                          required: true,
                          message: 'Please input your E-mail!',
                        },
                      ],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Password" hasFeedback>
                    {getFieldDecorator('password', {
                      rules: [
                        {
                          required: true,
                          message: 'Please input your password!',
                        },
                        {
                          validator: this.validateToNextPassword,
                        },
                      ],
                    })(<Input.Password />)}
                  </Form.Item>
                  <Form.Item label="Confirm Password" hasFeedback>
                    {getFieldDecorator('confirm', {
                      rules: [
                        {
                          required: true,
                          message: 'Please confirm your password!',
                        },
                        {
                          validator: this.compareToFirstPassword,
                        },
                      ],
                    })(<Input.Password onBlur={this.handleConfirmBlur} />)}
                  </Form.Item>

                  <Form.Item label="Phone Number">
                    {getFieldDecorator('tpnumber', {
                      rules: [{ required: true, message: 'Please input your phone number!' }],
                    })(<Input addonBefore={prefixSelector} style={{ width: '100%' }} />)}
                  </Form.Item>
                    
                  <Form.Item
                    label={
                      <span>
                        Address;
              
                      </span>
                    }
                  >
                    {getFieldDecorator('address', {
                      rules: [{ required: true, message: 'Please input your address!', whitespace: true }],
                    })(<Input />)}
                  </Form.Item>

                  <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">
                      Register
                    </Button>
                  </Form.Item>
                </Form>
              </div>
              <CustomFooter />
        </Layout>
      );
    }
  }
  
  const WrappedAddUserComponent = Form.create({ name: 'register' })(AddUserComponent);
  
export default WrappedAddUserComponent;
