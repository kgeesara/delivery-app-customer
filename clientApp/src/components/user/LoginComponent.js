import React from 'react';
import { Form, Icon, Input, Button, Alert, Layout } from 'antd';
import AuthService from '../../service/AuthService';
import NavbarBefore from '../NavbarBefore';
import CustomFooter from '../CustomFooter';
import RestaurantService from '../../service/RestaurantService';
const { Content } = Layout;

class LoginComponent extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            username: '',
            password: '',
            message: '',
        }
        this.login = this.login.bind(this);
    }

    componentDidMount() {
        localStorage.clear();
    }

//   login = (e) => {
//     e.preventDefault();
//     const credentials = {username: this.state.username, password: this.state.password};
//     AuthService.login(credentials).then(res => {
//         if(res.data.status === 200){
//             localStorage.setItem("userInfo", JSON.stringify(res.data.result));
//             this.props.history.push('/list-user');
//         }else {
//             this.setState({message: res.data.message});
//         }
//     });
// };

login = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const credentials = {username: values.username, password: values.password};
    AuthService.login(credentials).then(res => {
        if(res.data.status === 200){
          console.log(res.data.result);
            localStorage.setItem("userInfo", JSON.stringify(res.data.result));
            console.log(localStorage.getItem("userInfo"))
            this.props.history.push('/restaurantFeed');
            // RestaurantService.fetchFoods()
            // .then((response)  => {
            //   console.log("CCCCCCCCCCCCCCCCCCCCCCCCCC");
            //   console.log(response);
            //     localStorage.setItem("foodItems",JSON.stringify(response.data.result));
            // })
        }else {
            this.setState({message: res.data.message});
        }
    });
}
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
        <Layout>
            <NavbarBefore />
            <Content style={{ padding: '0 50px' , height: '100%'}}>
              <div style={{ background: '#fff', padding: 24 ,  height: '100%', textAlign: '-webkit-center'}}>Asiri Food Delivery
                <div>{this.state.message && <Alert message={this.state.message} type="error" />}</div>
                <Form style = {{maxWidth: '300px'}} onSubmit={this.login} className="login-form">
                  <Form.Item>
                    {getFieldDecorator('username', {
                      rules: [{ required: true, message: 'Please input your username!' }],
                    })(
                      <Input
                        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        placeholder="Username"
                      />,
                    )}
                  </Form.Item>
                  <Form.Item>
                    {getFieldDecorator('password', {
                      rules: [{ required: true, message: 'Please input your Password!' }],
                    })(
                      <Input
                        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        type="password"
                        placeholder="Password"
                      />,
                    )}
                  </Form.Item>
                  <Form.Item>
                    <Button style = {{width: '100%'}}type="primary" htmlType="submit" className="login-form-button">
                      Log in
                    </Button>
                    Or <a href="/register">register now!</a>
                  </Form.Item>
                </Form>
              </div>  
            </Content>
            <CustomFooter />
        </Layout>
    );
  }
}

const WrappedHorizontalLoginForm = Form.create({ name: 'horizontal_login' })(LoginComponent);



export default WrappedHorizontalLoginForm;
