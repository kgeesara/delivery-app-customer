import React, { Component } from 'react'
import UserService from "../../service/UserService";
import NavbarAfter from '../NavbarAfter';
import {
    Form,
    Input,
    Select,
    Button,
    Layout,
    Alert,
  } from 'antd';
import CustomFooter from '../CustomFooter';

const { Option } = Select;

class EditUserComponent extends Component {

    constructor(props){
        super(props);
        this.state ={
            id: '',
            username: '',
            email: '',
            address: '',
            tpnumber: '',
            message:null,
        }
        this.saveUser = this.saveUser.bind(this);
        // this.loadUser = this.loadUser.bind(this);
    }
    state = {
        confirmDirty: false,
        autoCompleteResult: [],
      };
    componentDidMount() {
        // this.loadUser();
        UserService.fetchUserByUserName(JSON.parse(window.localStorage.getItem("userInfo")).username)
        .then((res) => {
          console.log(res)
            let user = res.data.result;
            this.setInitialValues(user);
        })
        // this.setInitialValues();
    }

    setInitialValues = (user) => {
      const { form } = this.props;
      form.setFieldsValue({
        id: String(user.id),
        username: user.username,
        email: user.email,
        address: user.address,
        tpnumber: user.tpnumber,
      });
  };

    // loadUser() {
    //     UserService.fetchUserByUserName(JSON.parse(window.localStorage.getItem("userInfo")).username)
    //         .then((res) => {
    //           console.log(res)
    //             let user = res.data.result;
    //             this.setState({
    //             // id: user.id,
    //             username: user.username,
    //             email: user.email,
    //             address: user.address,
    //             tpnumber: user.tpnumber,
    //         })
    //         });
    // }

    // onChange = (e) =>
    //     this.setState({ [e.target.name]: e.target.value });

    // saveUser = (e) => {
    //     e.preventDefault();
    //     let user = {id: this.state.id, password: this.state.password, firstName: this.state.firstName, lastName: this.state.lastName, age: this.state.age, salary: this.state.salary};
    //     UserService.editUser(user)
    //         .then(res => {
    //             this.setState({message : 'User added successfully.'});
    //             this.props.history.push('/list-user');
    //         });
    // }
    saveUser = e => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) => {
        console.log(values);
        console.log("LLLLLLLL");
        if (!err) {
          let user = {id: String(values.id), username: values.username, email: values.email, address: values.address, tpnumber: values.tpnumber};
          UserService.editUser(user)
          .then(response => {
            console.log(response);
            this.setState({message : 'User added successfully.'});
          })
        }
      });
    };
    // render() {

    //     return (
    //         <React.Fragment>
    //             <NavbarAfter/>
    //             <Container>
    //                 <Typography variant="h4" style={style}>Edit User</Typography>
    //                 <form>

    //                     <TextField label="USERNAME" fullWidth margin="normal" name="username" value={this.state.username} disabled/>

    //                     <TextField label="FIRST NAME" fullWidth margin="normal" name="firstName" value={this.state.firstName} onChange={this.onChange}/>

    //                     <TextField label="LAST NAME" fullWidth margin="normal" name="lastName" value={this.state.lastName} onChange={this.onChange}/>

    //                     <TextField type="number" label="AGE" fullWidth margin="normal" name="age" value={this.state.age} onChange={this.onChange}/>

    //                     <TextField type="number" label="SALARY" fullWidth margin="normal" name="salary" value={this.state.salary} onChange={this.onChange}/>

    //                     <Button variant="contained" color="primary" onClick={this.saveUser}>Save</Button>

    //                 </form>
    //             </Container>
    //         </React.Fragment>
    //     );
    // }

    handleConfirmBlur = e => {
        const { value } = e.target;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
      };
    
    
      render() {
        const { getFieldDecorator } = this.props.form;
    
        const formItemLayout = {
          labelCol: {
            xs: { span: 12 },
            sm: { span: 8 },
          },
          wrapperCol: {
            xs: { span: 12 },
            sm: { span: 8 },
          },
        };
        const tailFormItemLayout = {
          wrapperCol: {
            xs: {
              span: 24,
              offset: 0,
            },
            sm: {
              span: 8,
              offset: 8,
            },
          },
        };
        const prefixSelector = getFieldDecorator('prefix', {
          initialValue: '94',
        })(
          <Select style={{ width: 70 }}>
            <Option value="94">+94</Option>
  
          </Select>
        );

    
        return (
          <Layout className="layout">
              <NavbarAfter />
              <div style={{ background: '#fff', padding: 24 ,  height: '100%', textAlign: '-webkit-center'}}>Asiri Food Delivery
                  <div>{this.state.message && <Alert message={this.state.message} type="success" />}</div>
                  <Form {...formItemLayout} onSubmit={this.saveUser}>
                  <Form.Item
                      label={
                        <span>
                          ID&nbsp;
                          
                        </span>
                      }
                      hidden={true}
                    >
                      {getFieldDecorator('id', {
                        rules: [{ required: true, whitespace: true }],
                      })(<Input readOnly/>)}
                    </Form.Item>
                    <Form.Item
                      label={
                        <span>
                          Username&nbsp;
                          
                        </span>
                      }
                    >
                      {getFieldDecorator('username', {
                        rules: [{ required: true, message: 'Please input your username!', whitespace: true }],
                      })(<Input readOnly/>)}
                    </Form.Item>
                    <Form.Item label="E-mail">
                      {getFieldDecorator('email', {
                        rules: [
                          {
                            type: 'email',
                            message: 'The input is not valid E-mail!',
                          },
                          {
                            required: true,
                            message: 'Please input your E-mail!',
                          },
                        ],
                      })(<Input />)}
                    </Form.Item>
  
                    <Form.Item label="Phone Number">
                      {getFieldDecorator('tpnumber', {
                        rules: [{ required: true, message: 'Please input your phone number!' }],
                      })(<Input addonBefore={prefixSelector} style={{ width: '100%' }} />)}
                    </Form.Item>
                      
                    <Form.Item
                      label={
                        <span>
                          Address;
                
                        </span>
                      }
                    >
                      {getFieldDecorator('address', {
                        rules: [{ required: true, message: 'Please input your address!', whitespace: true }],
                      })(<Input />)}
                    </Form.Item>
  
                    <Form.Item {...tailFormItemLayout}>
                      <Button type="primary" htmlType="submit">
                        Save Changes
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
                <CustomFooter />
          </Layout>
        );
      }
}

const WrappedEditUserComponent = Form.create({ name: 'register' })(EditUserComponent);
export default WrappedEditUserComponent;