import React from 'react';
import { Form, Button, Alert, Layout, Radio } from 'antd';
import CustomFooter from '../CustomFooter';
import UserService from '../../service/UserService';
import PaymentService from '../../service/paymentSevice';
import NavbarAfter from '../NavbarAfter';
const { Content } = Layout;

class PaymentForm extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            username: '',
            password: '',
            message: '',
            balance: 0,
            buttonDisable: true,
        }
        this.pay = this.pay.bind(this);
    }

    componentDidMount() {
        UserService.fetchUserByUserName(JSON.parse(window.localStorage.getItem("userInfo")).username)
        .then((res) => {
          console.log(res)
            let user = res.data.result;
            this.setState({balance:user.transactionStatus})
            if(user.transactionStatus>0){
                this.setState({buttonDisable: false})
            }
        })

    }

pay = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
        console.log(values);
      if (!err) {
        const details = {paymentMethod: values.paymentMethod, amount: this.state.balance,customerId: JSON.parse(window.localStorage.getItem("userInfo")).id};
        PaymentService.pay(details).then(res => {
        if(res.data.status === 200){
          console.log(res.data.result);
          let user = {username: JSON.parse(window.localStorage.getItem("userInfo")).username, transactionStatus: 0}
          UserService.editUserTransactionStatus(user)
          .then((res) => {
            console.log(res);
            this.setState({message: res.data.message, balance: 0, buttonDisable: true});
          })
        }
    });
}
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
        <Layout>
            <NavbarAfter />
            <Content style={{ padding: '0 50px' , height: '100%'}}>
                <div style={{ background: '#fff', padding: 24 ,  height: '100%', textAlign: '-webkit-center'}}>Asiri Food Delivery
                    <div>{this.state.message && <Alert message={this.state.message} type="success" />}</div>
                    <div>
                        Amount to be paid : {this.state.balance}
                    </div>
                    <Form style = {{maxWidth: '300px'}} onSubmit={this.pay} className="login-form">
                        <Form.Item label="Payment Method">
                            {getFieldDecorator('paymentMethod', {
                      rules: [{ required: true, message: 'Please select a payment method!', whitespace: true }],
                    })(
                            <Radio.Group>
                                <Radio value="cash">Cash</Radio>
                                <Radio value="credit card">Cedi Card</Radio>
                            </Radio.Group>,
                            )}
                        </Form.Item>
                        
                        <Form.Item>
                            <Button style = {{width: '100%'}}type="primary" htmlType="submit" className="login-form-button" disabled={this.state.buttonDisable}>
                            Pay Now
                            </Button>
                        </Form.Item>
                    </Form>
                </div>  
            </Content>
            <CustomFooter />
        </Layout>
    );
  }
}

const WrappedPaymentForm = Form.create({ name: 'payment' })(PaymentForm);



export default WrappedPaymentForm;
