import React from 'react';
import { Layout } from 'antd';

const { Footer } = Layout;

function CustomFooter() {
    return (
      
            <Footer style={{ textAlign: 'center', bottom: '0px', position: 'fixed', width: '100%'}}>ACCElAERO Design ©2019 Created by ACCElAERO</Footer>
    );
}

export default CustomFooter;