import React from 'react';
import { Layout, Menu } from 'antd';
import { Link } from 'react-router-dom';
import './navbar.css'
import { render } from '@testing-library/react';
const { Header } = Layout;


 class NavbarAfter extends React.Component{
   logout(){
    localStorage.clear();
   }
   render(){

    return(
      // <nav className="nav-wrapper">
      //     <div className="container">
      //         <Link to="/" className="brand-logo">Shopping</Link>
              
      //         <ul className="right">
      //             <li><Link to="/">Shop</Link></li>
      //             <li><Link to="/cart">My cart</Link></li>
      //             <li><Link to="/cart"><i className="material-icons">shopping_cart</i></Link></li>
      //         </ul>
      //     </div>
      // </nav>

      <Header>
      <div className="logo" />
      <Menu
        theme="dark"
        mode="horizontal"
        // defaultSelectedKeys={['1']}
        style={{ lineHeight: '64px' }}
        
      >
        <Menu.Item className="customclass"  key="2"><Link to={'/restaurantFeed'} className="nav-link"> Restaurant Feed </Link></Menu.Item>
        <Menu.Item className="customclass" key="3"><Link to={'/cart'} className="nav-link"> My Cart </Link></Menu.Item>
        <Menu.Item className="customclass" key="4"><Link to={'/editProfile'} className="nav-link"> My Profile </Link></Menu.Item>
        <Menu.Item className="customclass" key="5"><Link to={'/payment'} className="nav-link"> Payment </Link></Menu.Item>
        <Menu.Item className="customclass" key="1"><Link onClick={this.logout} to={'/'} className="nav-link"> Log Out </Link></Menu.Item>
         {/* <Menu.Item key="4"><Link to={'/restaurantFeed'} className="nav-link"> Restaurant Feed </Link></Menu.Item>
        <Menu.Item key="5"><Link to={'/cart'} className="nav-link"> My Cart </Link></Menu.Item> */}
      </Menu>
    </Header>

  
)

   }
    
}

export default NavbarAfter;