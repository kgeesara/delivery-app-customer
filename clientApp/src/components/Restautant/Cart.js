import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { removeItem,addQuantity,subtractQuantity} from '../actions/cartActions';
import Recipe from './Recipe';
import { Icon, Button,Layout } from 'antd';
import NavbarAfter from '../NavbarAfter';
import CustomFooter from '../CustomFooter';
import Item7 from '../../images/item7.jpg'
const { Content} = Layout;

class Cart extends Component{

    //to remove the item completely
    handleRemove = (id)=>{
        this.props.removeItem(id);
    }
    //to add the quantity
    handleAddQuantity = (id)=>{
        this.props.addQuantity(id);
    }
    //to substruct from the quantity
    handleSubtractQuantity = (id)=>{
        this.props.subtractQuantity(id);
    }
    render(){
              
        let addedItems = this.props.items.length ?
            (  
                this.props.items.map(item=>{
                    return(

                            <li key={item.id}>
                                                <div className="item-img"> 
                                                    <img src={Item7} alt={item.img} className=""/>
                                                </div>
                                            
                                                <div className="item-desc" style={{alignSelf:'center'}}>
                                                    <span className="title"  style={{fontSize:'20px',fontWeight:'500', color:'#ee6e73', display:'block', marginBottom:'20px'}}>{item.title}</span>
                                                    <p>{item.desc}</p>
                                                    <p><b>Price: {item.price}$</b></p> 
                                                    <p>
                                                        <b>Quantity: {item.quantity}</b> 
                                                    </p>
                                                    <div className="add-remove">
                                                        <Link to="/cart"><Icon type="caret-up" onClick={()=>{this.handleAddQuantity(item.id)}}/></Link>
                                                        <Link to="/cart"><Icon type="caret-down" onClick={()=>{this.handleSubtractQuantity(item.id)}} /></Link>
                                                    </div>
                                                    <Button type="primary" size='large' onClick={()=>{this.handleRemove(item.id)}}>Remove</Button>
                                                </div>
                                                
                                    </li>
                         
                    )
                })
            ):

             (
                <p>Nothing.</p>
             )
       return(
        <Layout>
            <NavbarAfter />
            <Content style={{ padding: '0 50px' , height: '100%'}}>
                <div style={{ background: '#fff', padding: 24 ,  height: '100%', textAlign: '-webkit-center'}}>Asiri Food Delivery
            
                    <div className="container">
                        <div className="cart">
                            <h5>You have ordered:</h5>
                            <ul className="collection">
                                {addedItems}
                            </ul>
                        </div> 
                        <Recipe />          
                    </div>
                </div>
            </Content>
            <CustomFooter />
        </Layout>
       )
    }
}


const mapStateToProps = (state)=>{
    return{
        items: state.addedItems,
        //addedItems: state.addedItems
    }
}
const mapDispatchToProps = (dispatch)=>{
    return{
        removeItem: (id)=>{dispatch(removeItem(id))},
        addQuantity: (id)=>{dispatch(addQuantity(id))},
        subtractQuantity: (id)=>{dispatch(subtractQuantity(id))}
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Cart)