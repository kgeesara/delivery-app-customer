import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Alert } from 'antd';
import OrderService from '../../service/OrderService';
import UserService from '../../service/UserService';
import { removeItem,addQuantity,subtractQuantity} from '../actions/cartActions';

class Recipe extends Component{
    
    // componentWillUnmount() {
    //      if(this.refs.shipping.checked)
    //           this.props.substractShipping()
    // }
    state= {message:null, buttonDisable: true}
    componentDidMount(){
        if(this.props.addedItems.length>0){
            this.setState({buttonDisable: false});
        }
    }

    handleChecked = (e)=>{
        if((this.props.addedItems.length)>0){
            const order = {customerId: JSON.parse(window.localStorage.getItem("userInfo")).id, foodId: this.props.addedItems[0].id, restaurantId: this.props.addedItems[0].res_id, unitPrice: this.props.addedItems[0].price, quantity: this.props.addedItems[0].quantity, total: this.props.total};
            OrderService.saveOrder(order)
            .then(response => {
                let user = {username: JSON.parse(window.localStorage.getItem("userInfo")).username, transactionStatus: this.props.total}
                UserService.editUserTransactionStatus(user)
                .then((res) => {
                  this.setState({message:"Ordered Suucessfully", buttonDisable: true});
                  this.props.removeItem(this.props.addedItems[0].id);
                })
            })
        }
    }

    render(){
  
        return(

                    
                       <div className="container">
                           <div>{this.state.message && <Alert message={this.state.message} type="success" />}</div>
                            <div className="collection">
                                <li className="collection-item" style={{display:'flex'}}><b>Total: {this.props.total} $</b></li>
                            </div>
                                <div className="checkout">
                                    <Button type="primary" size='large' disabled={this.state.buttonDisable} onClick={this.handleChecked}>Check Out</Button>
                                </div>
                        </div>
        )
    }
}

const mapStateToProps = (state)=>{
    return{
        addedItems: state.addedItems,
        total: state.total
    }
}
const mapDispatchToProps = (dispatch)=>{
    return{
        removeItem: (id)=>{dispatch(removeItem(id))},
        addQuantity: (id)=>{dispatch(addQuantity(id))},
        subtractQuantity: (id)=>{dispatch(subtractQuantity(id))}
    }
}

// const mapDispatchToProps = (dispatch)=>{
//     return{
//         addShipping: ()=>{dispatch({type: 'ADD_SHIPPING'})},
//         substractShipping: ()=>{dispatch({type: 'SUB_SHIPPING'})}
//     }
// }

export default connect(mapStateToProps,mapDispatchToProps)(Recipe)
