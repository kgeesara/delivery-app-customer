import React from 'react';
import RestaurantDetail from './restaurantDetail';
import RestaurantService from '../../service/RestaurantService';
import NavbarAfter from '../NavbarAfter';
import { Layout } from 'antd';
import CustomFooter from '../CustomFooter';
const { Content } = Layout;

class RestaurantFeed extends React.Component{

    state = {restaurant : []}

    componentDidMount (){
        RestaurantService.fetchRestaurants().then(res => {
            console.log(res);
            if(res.data.status === 200){
                console.log(res.data);
                this.setState({restaurant : res.data.result})
            }else {
                this.setState({message: res.data.message});
            }
        })
        RestaurantService.fetchFoods()
            .then((response)  => {
              console.log("CCCCCCCCCCCCCCCCCCCCCCCCCC");
              console.log(response);
                localStorage.setItem("foodItems",JSON.stringify(response.data.result));
            })
    }
    // login = e => {
    //     e.preventDefault();
    //     this.props.form.validateFieldsAndScroll((err, values) => {
    //       if (!err) {
    //         const credentials = {username: values.username, password: values.password};
    //     AuthService.login(credentials).then(res => {
    //         if(res.data.status === 200){
    //             localStorage.setItem("userInfo", JSON.stringify(res.data.result));
    //             this.props.history.push('/restaurantFeed');
    //         }else {
    //             this.setState({message: res.data.message});
    //         }
    //     });
    // }
    //     });
    //   };
    render(){

        return(
            <Layout>
            <NavbarAfter />
            <Content style={{ padding: '0 50px' , height: '100%'}}>
              <div style={{ background: '#fff', padding: 10 ,  height: '100%', textAlign: '-webkit-center'}}>Asiri Food Delivery
              
            <RestaurantDetail detail={this.state.restaurant} />
            </div>
            </Content>
            <CustomFooter />
            </Layout>
            // <ul>
            // {
            //     this.state.restaurant.map((eachCourse) => {
            //         return <li key={eachCourse.id}>{eachCourse.name}</li>
            //     })
            // }
            // </ul>
        );

    }

}
export default RestaurantFeed;