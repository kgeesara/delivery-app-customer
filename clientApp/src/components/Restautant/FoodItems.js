import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addToCart } from '../actions/cartActions';
import { Card, Icon, Col, Layout, Alert } from 'antd';
import NavbarAfter from '../NavbarAfter';
import CustomFooter from '../CustomFooter';
import Item7 from '../../images/item7.jpg'
import UserService from '../../service/UserService';
import RestaurantService from '../../service/RestaurantService';
const { Content } = Layout;

 class FoodItems extends Component{

    state = {addButtom : false, oneOrder:false, itemsAddedBedore: false}
    
    handleClick = (id)=>{
        console.log(this.state);
        console.log("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL")
        UserService.fetchUserByUserName(JSON.parse(window.localStorage.getItem("userInfo")).username)
        .then((res) => {
          console.log(res)
            let user = res.data.result;
            this.setState({balance:user.transactionStatus})
            if(user.transactionStatus>0){
                this.setState({addButtom: true});
            }else{
                if(this.state.oneOrder){
                    this.setState({itemsAddedBedore:true});
                } else{
                    this.props.addToCart(id);
                    this.setState({oneOrder:true})
                }
            }
        })
    }

    render(){
        const {foo} = this.props.location.state;

        let i=0;
        let itemList = this.props.items.map(item=>{
            i+=1;
            return(
                <div key={i}>{foo==item.res_id &&
                         <div className="card" key={item.id}  style={{ background: '#ECECEC', marginTop: '10px'}}>
                            <Col span={8}>
                                <Card style={{marginTop:'10px',marginRight:'10px'}}>
                                <div className="card-image">
                                <img src={Item7} alt={item.title} style={{width:'200px'}}/>
                                        {/* <span className="card-title">{item.title}</span> */}
                            </div>

                                    <div className="card-content">
                                    <span className="card-title" style={{fontWeight:"bold"}}>{item.title}</span>
                                        <p>{item.desc}</p>
                                        <p><b>Price: {item.price}$</b></p>
                                        <Icon type="plus-circle" style={{fontSize:'2.4em', cursor:'pointer', color:'red'}} onClick={()=>{this.handleClick(item.id)}}/>
                                    </div>
                                </Card>
                            </Col>
                            </div>
                        
                }</div>

            )
        })

        return(
            <Layout>
                <NavbarAfter />
                <Content style={{ padding: '0 50px' , height: '100%'}}>
                    <div style={{ background: '#fff', padding: 24 ,  height: '100%', textAlign: '-webkit-center'}}>Asiri Food Delivery
                       
                        <div className="container">
                            <h3 className="center">Our items</h3>
                            <div>{this.state.addButtom && <Alert message="Only 1 order per time" type="error" />}</div>
                            <div>{this.state.itemsAddedBedore && <Alert message="Only 1 item canbe ordered per time" type="error" />}</div>
                            <div className="box">
                                {itemList}
                            </div>
                        </div>
                    </div>
                </Content>
                <CustomFooter />
            </Layout>
        )
    }
}
const mapStateToProps = (state)=>{
    state.items = JSON.parse(window.localStorage.getItem("foodItems"));
    console.log(JSON.parse(window.localStorage.getItem("foodItems")));
    console.log("KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKkk");
    return {
      items: state.items
    }
  }
const mapDispatchToProps= (dispatch)=>{
    
    return{
        addToCart: (id)=>{dispatch(addToCart(id))}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(FoodItems)