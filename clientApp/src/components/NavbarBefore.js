import React from 'react';
import { Layout, Menu } from 'antd';
import { Link } from 'react-router-dom';
const { Header } = Layout;
 const NavbarBefore = ()=>{
    return(
            // <nav className="nav-wrapper">
            //     <div className="container">
            //         <Link to="/" className="brand-logo">Shopping</Link>
                    
            //         <ul className="right">
            //             <li><Link to="/">Shop</Link></li>
            //             <li><Link to="/cart">My cart</Link></li>
            //             <li><Link to="/cart"><i className="material-icons">shopping_cart</i></Link></li>
            //         </ul>
            //     </div>
            // </nav>

            <Header>
            <div className="logo" />
            <Menu
              theme="dark"
              mode="horizontal"
              //defaultSelectedKeys={['1']}
              style={{ lineHeight: '64px' }}
            >
              <Menu.Item key="1"><Link to={'/'} className="nav-link"> Home </Link></Menu.Item>
              <Menu.Item key="2"><Link to={'/login'} className="nav-link"> Login </Link></Menu.Item>
              <Menu.Item key="3"><Link to={'/register'} className="nav-link"> Register </Link></Menu.Item>
              {/* <Menu.Item key="4"><Link to={'/restaurantFeed'} className="nav-link"> Restaurant Feed </Link></Menu.Item>
              <Menu.Item key="5"><Link to={'/cart'} className="nav-link"> My Cart </Link></Menu.Item> */}
            </Menu>
          </Header>
   
        
    )
}

export default NavbarBefore;