// import React from 'react';
// import { Layout, Menu, Breadcrumb } from 'antd';
// import WrappedRegistrationForm from './registerForm';
// import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
// import WrappedHorizontalLoginForm from './loginForm';
// import carousel from './carousel';
// import RestaurantDetail from './restaurantDetail';
// import RestaurantFeed from './restaurantFeed';
// import Cart from './components/Cart';
// import FoodItems from './components/FoodItems';

// const { Header, Content, Footer } = Layout;

// function LayoutCustom() {
//     return (
//       <Router>
//           <Layout className="layout">
//             <Header>
//               <div className="logo" />
//               <Menu
//                 theme="dark"
//                 mode="horizontal"
//                 //defaultSelectedKeys={['1']}
//                 style={{ lineHeight: '64px' }}
//               >
//                 <Menu.Item key="1"><Link to={'/'} className="nav-link"> Home </Link></Menu.Item>
//                 <Menu.Item key="2"><Link to={'/login'} className="nav-link"> Login </Link></Menu.Item>
//                 <Menu.Item key="3"><Link to={'/register'} className="nav-link"> Register </Link></Menu.Item>
//                 <Menu.Item key="4"><Link to={'/restaurantFeed'} className="nav-link"> Restaurant Feed </Link></Menu.Item>
//                 <Menu.Item key="5"><Link to={'/cart'} className="nav-link"> My Cart </Link></Menu.Item>
//               </Menu>
//             </Header>
//             <Content style={{ padding: '0 50px' , height: '100%'}}>
//               <div style={{ background: '#fff', padding: 24 ,  height: '100%', textAlign: '-webkit-center'}}>Asiri Food Delivery
//                   <Switch>
//                     <Route exact path='/' component={carousel} />
//                     <Route exact path='/login' component={WrappedHorizontalLoginForm} />
//                     <Route exact path='/register' component={WrappedRegistrationForm} />
//                     <Route exact path='/restaurantFeed' component={RestaurantFeed} />
//                     <Route exact path='/restaurantFoods' component={FoodItems} />
//                     <Route exact path='/cart' component={Cart} />
              
//                   </Switch>
//                 </div>
              
//             </Content>
//             <Footer style={{ textAlign: 'center', bottom: '0px' }}>ACCElAERO Design ©2019 Created by ACCElAERO</Footer>
//           </Layout>

//           </Router>
//     );
// }

// export default LayoutCustom;