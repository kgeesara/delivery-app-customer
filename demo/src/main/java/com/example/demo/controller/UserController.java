package com.example.demo.controller;

import com.example.demo.model.ApiResponse;
import com.example.demo.model.Customer;
import com.example.demo.model.CustomerDto;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public ApiResponse<Customer> saveUser(@RequestBody CustomerDto customerDto){
        return new ApiResponse<>(HttpStatus.OK.value(), "Userzzzz saved successfully.",userService.save(customerDto));
    }

    @GetMapping
    public ApiResponse<List<Customer>> listUser(){
        return new ApiResponse<>(HttpStatus.OK.value(), "Userzzzz list fetched successfully.",userService.findAll());
    }

//    @GetMapping("/{id}")
//    public ApiResponse<Customer> getOne(@PathVariable int id){
//        return new ApiResponse<>(HttpStatus.OK.value(), "Userzzzz fetched successfully.",userService.findById(id));
//    }

    @GetMapping("/{username}")
    public ApiResponse<Customer> getOne1(@PathVariable String username){
        return new ApiResponse<>(HttpStatus.OK.value(), "Userzzzz fetched successfully.",userService.findOne(username));
    }

    @PutMapping("/{username}")
    public ApiResponse<CustomerDto> update(@RequestBody CustomerDto customerDto) {
        return new ApiResponse<>(HttpStatus.OK.value(), "Userzzzz updated successfully.",userService.update(customerDto));
    }

    @PutMapping("/transactionStatus/{username}")
    public ApiResponse<CustomerDto> updateTransactionStatus(@RequestBody CustomerDto customerDto) {
        return new ApiResponse<>(HttpStatus.OK.value(), "Transaction Status updated successfully.",userService.upddateTransactionStatus(customerDto));
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable int id) {
        userService.delete(id);
        return new ApiResponse<>(HttpStatus.OK.value(), "Userzzzz deleted successfully.", null);
    }



}

