package com.example.demo.controller;

import com.example.demo.engine.Producer;
import com.example.demo.model.ApiResponse;
import com.example.demo.model.Order;
import com.example.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
public class OrderController {
    @Autowired
    OrderService orderService;

    @Autowired
    Producer producer;

    @PostMapping("/order")
    public ApiResponse<Order> saveOrder(@RequestBody Order order){
        producer.send2(order);
        return new ApiResponse<>(HttpStatus.OK.value(), "Order saved successfully.",orderService.save(order));
    }
}
