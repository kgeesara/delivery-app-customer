package com.example.demo.controller;

import com.example.demo.model.ApiResponse;
import com.example.demo.model.Payment;
import com.example.demo.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @PostMapping("/pay")
    public ApiResponse<Payment> savePayment(@RequestBody Payment payment){
        return new ApiResponse<>(HttpStatus.OK.value(), "Paid successfully.",paymentService.save(payment));
    }

}
