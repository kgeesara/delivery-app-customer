package com.example.demo.controller;

import com.example.demo.model.ApiResponse;
import com.example.demo.model.Customer;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController
public class RestaurantController {

    @GetMapping(path = "/restaurants", produces= MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse<ArrayList> sayHello() throws JSONException {
        ArrayList<HashMap> arrayList = new ArrayList<>();
        for(int i=0;i<=22;i+=1){
            HashMap<String, String> map = new HashMap<>();

            map.put("id", ""+i);
            map.put("name", "restaurant"+ i);
            arrayList.add(map);
        }

        return new ApiResponse<>(HttpStatus.OK.value(), "Userzzzz list fetched successfully.",arrayList);


    }

    @GetMapping(path = "/foods", produces= MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse<ArrayList> getFoods() throws JSONException {
        ArrayList<HashMap> arrayList = new ArrayList<>();
//        items: [
//        {id="1",title:'Winter body', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:110,img:Item1},
//        {id:2,title:'Adidas', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:80,img: Item2},
//        {id:3,title:'Vans', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.",price:120,img: Item3},
//        {id:4,title:'White', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:260,img:Item4},
//        {id:5,title:'Cropped-sho', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:160,img: Item5},
//        {id:6,title:'Blues', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.",price:90,img: Item6}
//    ],
        for(int i=0;i<=22;i+=1){
            HashMap<String, Object> map = new HashMap<>();

            map.put("res_id", i);
            map.put("id", i+"-"+1);
            map.put("title", "Rice & Curry"+i);
            map.put("desc", "Red rice with chicjen and 4 curries");
            map.put("price", 150*i);
            map.put("img", "'../../images/item1.jpg'");
            arrayList.add(map);
        }

        HashMap<String, Object> map = new HashMap<>();

        map.put("res_id", 1);
        map.put("id", 1+"-"+2);
        map.put("title", "Rice & Curry"+444);
        map.put("desc", "Red rice with chicjen and 4 curries");
        map.put("price", 150);
        map.put("img", "'../../images/item1.jpg'");
        arrayList.add(map);

        return new ApiResponse<>(HttpStatus.OK.value(), "UXXXXXXXXXXXXXXXXXXXXXXXXXXXX.",arrayList);


    }
}
