package com.example.demo.model;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int orderId;
    @Column
    private int customerId;
    @Column
    private int restaurantId;
    @Column
    private String foodId;
    @Column
    private int unitPrice;
    @Column
    private int quantity;
    @Column
    private int total;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ordered_at", nullable = false, updatable = false)
    @CreatedDate
    private Date orderedAt;

    public Order() {
    }

    public Order(int customerId, int restaurantId, String foodId, int unitPrice, int quantity, int total, Date orderedAt) {
        this.customerId = customerId;
        this.restaurantId = restaurantId;
        this.foodId = foodId;
        this.unitPrice = unitPrice;
        this.quantity = quantity;
        this.total = total;
        this.orderedAt = orderedAt;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getFoodId() {
        return foodId;
    }

    public void setFoodId(String foodId) {
        this.foodId = foodId;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public Date getOrderedAt() {
        return orderedAt;
    }

    public void setOrderedAt(Date orderedAt) {
        this.orderedAt = orderedAt;
    }

    @Override
    public String toString() {
        return "Order{" +
                ", restaurantId=" + restaurantId +
                ", foodId='" + foodId + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
