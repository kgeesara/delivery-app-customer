package com.example.demo.engine;

import com.example.demo.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Producer {

    @Autowired
    private KafkaTemplate<String, Order> kafkaTemplate;

//    String kafkaTopic = "java_in_use_topic";
//
//    public void send(Order order) {
//
//        kafkaTemplate.send(kafkaTopic, order);
//
//    }

    String kafkaTopic2 = "order_topic";

    public void send2(Order order) {

        kafkaTemplate.send(kafkaTopic2, order);

    }
}
