package com.example.demo.engine;

import com.example.demo.model.Order;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;

@Service
public class Consumer {
    public  static ArrayList<String> orderArrayList = new ArrayList<>();
//    @KafkaListener(topics = "java_in_use_topic", groupId = "Group_Id")
//    public void consume(String message) throws IOException {
//        System.out.println(String.format("#### -> Consumed message -> %s", message));
//    }

    @KafkaListener(topics = "order_topic", groupId = "Group_Id5",
            containerFactory = "orderKafkaListenerFactory")
    public void consumeJson(Order order) {
        orderArrayList.add(order.toString());
        System.out.println("Consumed JSON Message: " + orderArrayList);
    }
}
