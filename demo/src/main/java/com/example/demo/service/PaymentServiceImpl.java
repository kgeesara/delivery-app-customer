package com.example.demo.service;

import com.example.demo.dao.PaymentDao;
import com.example.demo.model.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Timestamp;

@Service(value = "paymentService")
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    PaymentDao paymentDao;

    @Autowired
    UserService userService;

    @Override
    public Payment save(Payment payment) {
        Payment newPayment = new Payment();
        newPayment.setCustomerId(payment.getCustomerId());
        newPayment.setAmount(payment.getAmount());
        newPayment.setPaymentMethod(payment.getPaymentMethod());
        newPayment.setPayedAt(new Timestamp(System.currentTimeMillis()));
        return paymentDao.save(newPayment);
    }
}
