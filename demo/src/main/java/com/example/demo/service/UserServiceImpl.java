package com.example.demo.service;

import com.example.demo.dao.CustomerDao;
import com.example.demo.model.Customer;
import com.example.demo.model.CustomerDto;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Customer customer = customerDao.findByUsername(username);
        if(customer == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(customer.getUsername(), customer.getPassword(), getAuthority());
    }

    private List<SimpleGrantedAuthority> getAuthority() {
        return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }

    public List<Customer> findAll() {
        List<Customer> list = new ArrayList<>();
        customerDao.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(int id) {
        customerDao.deleteById(id);
    }

    @Override
    public Customer findOne(String username) {
        return customerDao.findByUsername(username);
    }

    @Override
    public Customer findById(int id) {
        Optional<Customer> optionalUser = customerDao.findById(id);
        return optionalUser.isPresent() ? optionalUser.get() : null;
    }

    @Override
    public CustomerDto update(CustomerDto customerDto) {
        Customer customer = findOne(customerDto.getUsername());
        if(customer != null) {
            BeanUtils.copyProperties(customerDto, customer, "password", "username", "transactionStatus");
            customerDao.save(customer);
        }
        return customerDto;
    }

    @Override
    public CustomerDto upddateTransactionStatus(CustomerDto customerDto){
        Customer customer = findOne(customerDto.getUsername());
        customer.setTransactionStatus(customerDto.getTransactionStatus());
        customerDao.save(customer);
        return customerDto;
    }

    @Override
    public Customer save(CustomerDto user) {
        Customer newCustomer = new Customer();
        newCustomer.setUsername(user.getUsername());
        newCustomer.setEmail(user.getEmail());
        newCustomer.setAddress(user.getAddress());
        newCustomer.setPassword(bcryptEncoder.encode(user.getPassword()));
        newCustomer.setTransactionStatus(user.getTransactionStatus());
        newCustomer.setTpnumber(user.getTpnumber());
        return customerDao.save(newCustomer);
    }
}

