package com.example.demo.service;

import com.example.demo.model.Customer;
import com.example.demo.model.CustomerDto;

import java.util.List;

public interface UserService {

    Customer save(CustomerDto user);
    List<Customer> findAll();
    void delete(int id);

    Customer findOne(String username);

    Customer findById(int id);

    CustomerDto update(CustomerDto customerDto);

    CustomerDto upddateTransactionStatus(CustomerDto customerDto);
}