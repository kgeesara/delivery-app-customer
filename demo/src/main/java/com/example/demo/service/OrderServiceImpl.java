package com.example.demo.service;

import com.example.demo.dao.OrderDao;
import com.example.demo.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service(value = "orderService")
public class OrderServiceImpl implements  OrderService {

    @Autowired
    OrderDao orderDao;

    @Override
    public Order save(Order order) {
        Order newOrder = new Order();
        newOrder.setCustomerId(order.getCustomerId());
        newOrder.setFoodId(order.getFoodId());
        newOrder.setRestaurantId(order.getRestaurantId());
        newOrder.setUnitPrice(order.getUnitPrice());
        newOrder.setQuantity(order.getQuantity());
        newOrder.setTotal(order.getTotal());
        newOrder.setOrderedAt(new Timestamp(System.currentTimeMillis()));
        return orderDao.save(newOrder);
    }
}
